Ce guide a pour but de présenter les étapes nécessaires à la mise en place de mon serveur.

> Ce qui guide à été écrit il y a plusieurs années, et possède de nombreux points d'amélioraions.
> De plus l'infrastructure mise en place à l'époque n'est pas assez robuste, du fait de la non présence de Docker.
> Une mise à jour du serveur ainsi que de ce guide est prévuee.

> Merci de regarder la fin du fichier

### sommaire
  1. [Identifier les besoins](#étape-1)
  2. [Mise en place du server ssh](#étape-2)
  3. [Mise en place du serveur http, la base de donné, et de php](#étape-3)
  4. [Sécurisation avec SSL](#étape-4)
  5. [Mise en place d'un serveur SMTP](#étape-5)
  6. [Surveillance des logs avec fail2ban](#étape-6)
  7. [Installation de Wordpress](#étape-7)
  8. [Installation du stockage Cloud](#étape-8)
  9. [Les amélioraions à effectuer](#et-après)

# Étape 1
#### Identifier les besoins
Pour un serveur de cette envergure, très simple, il y a peu nécessité à mettre en place.

Tout d'abord il me faut un **accès sécurisé à distance**,
une connexion ssh est parfaite pour cette application.
Ensuite j'aimerai **recevoir des notifications** lorsque qu'une personne envois un message sur le site.


###### [Revenier en haut](#top) 

# Étape 2
#### Mise en place su serveur ssh
Même si les options de base sont sécurisées, il est possible de renforcer ces mesures.

La première chose à faire est de créer une paire de clés et de désactiver la connexion par mot de passe.

Au moment de l'écriture de ce guide,
les clés les plus sûres et performantes sont les clés ed25519,
le seul inconvégniant est que ce type de clé n'est pas supporté par les systèmes plus anciens.

```
ssh-keygen -t ed25519
```

Une fois générée, on copie la clé sur le serveur.

```
ssh-copy-id -i ~/.ssh/id_ed25519 user@domain
```

Il ne reste plus qu'à interdire les connexions ssh par mot de passe.
On change donc les paramètres du serveur ssh dans le fichier `/etc/ssh/sshd_config`.
On modifie les lignes suivantes:

```
PermitRootLogin no
PasswordAuthentification no
```

Il est aussi conseillé d'utiliser un port différent de 22, car ce dernier est le plus scanné sur internet,
cependant, cela ne rajoute aucune sécurité.

###### [Revenier en haut](#top) 

# Étape 3
#### Mise en place du serveur http, la base de donné, et de php
 * De nombreux serveurs http sont disponibles,
deux d'entre eux sont les plus connus, apache2 et Nginx.
Le choix du serveur dépend de votre préférence.
Cependant, je ne présenterais que les commandes pour apache, car je ne suis pas familier à Nginx.

 * Pour la base de donné, là aussi il y a deux choix populaires.
On peut choisir entre MariaDB et MySQL.
Les deux options sont très similaires, la principale différence est que MariaDB est un projet OpenSource,
je vais donc choisir ce dernier pour cette raison.

 * Enfin il ne reste qu'à installer php et ses modules complémentaires pour qu'il s'intègre avec les autres applications.

Voici la commande pour installer tous les paquets nécessaires.

```shell
sudo apt install apache2 mariadb-server php libapache2-mod-php php-mysql  
```

On peut aussi installer des applications pour gérer les bases de données via le web, tel de PHPmyadmin.
Cependant, je ne vais pas en parler dans ce guide.

###### [Revenier en haut](#top) 

# Étape 4
#### Sécurisation avec SSL
L'utilisation du protocole HTTP est très fortement déconseillé.
C'est pour cela que l'on va faire les modifications nécessaires pour utiliser HTTPS.
>Attention, cela ne rend pas le serveur complètement sécurisé.
>Je vous invites a regarder par vous même les étapes supplémentaires pour augmenter la sécurité.

L'obtention de certificats est très simple.
Pour se faire, on utilisera [Certbot](https://certbot.eff.org/).
Il s'agit d'un outil qui installe automatiquement des certificats,
et de le mettre en place pour le serveur http.

On suit donc les instructions sur le site. Une fois les commandes rentrées, on suit les instructions sur le terminal.

```shell
sudo apt install certbot python-certbot-apache
sudo certbot --apache
```

Une fois fini, Cerbot va installer les certificats dans `/etc/letsencrypt/live/<domaine>/`
On s'en servira plus tard pour le serveur SMTP.

###### [Revenier en haut](#top) 

# Étape 5
#### Mise en place d'un serveur SMTP

Comme pour tout, le choix d'un serveur SMTP est vaste,
donc par question de simplicité, je vais utiliser postfix

```shell
sudo apt install postfix
```
On choisi "Site Internet" car il correspond à mon cas.
![postfix](postfix.png "Site Internet")

Dans le fichier de configuration `/etc/postfix/main.cf`

```shell
smtpd_tls_cert_file=/etc/letsencrypt/live/<domaine>/fullchain.pem
smtpd_tls_key_file=/etc/letsencrypt/live/<domain>/privkey.pem
smtpd_relay_restrictions = permit_sasl_authenticated,permit_mynetworks,reject_unauth_destination

mydomain = <domain>
myhostname = <domain>
mydestination = $myhostname, localhost.$mydomain, $mydomain
```
---

Cette configuration devrait suffire si vous pouvez utiliser le port 25.
Cependant, je possède Orange comme opérateur, et ce dernier ne permet pas l'utilisation de ce port.
Pour envoyer des mails, il faut passer par le serveur STMP de Orange.
Voici la configuration que j'ai donc mise en place

>`/etc/postfix/main.cf`
```shell
# configuration sous orange
smtp_sasl_password_maps = hash:/etc/postfix/sasl/mdp_fai.conf
smtp_sender_dependent_authentication = yes
smtp_sasl_auth_enable = yes
smtpd_sasl_authenticated_header = yes
broken_sasl_auth_clients = yes
smtp_sasl_security_options = noanonymous
smtp_tls_security_level = encrypt
smtpd_tls_req_ccert = yes
smtp_tls_wrappermode = yes
relayhost = [smtp.orange.fr]:465
```
>`/etc/postfix/sasl/mdp_fai.conf`
```shell
[smtp.orange.fr]:465 <E-mailOrange>:<MotDePasse>
```
Une fois la configuration créée, on intègre le mot de passe du compte Orange à Postfix.
On exécute les commandes suivantes:
```shell
sudo postmap /etc/postfix/sasl/mdp_fai.conf
sudo postconf -e smtp_sasl_password_maps=hash:/etc/postfix/sasl/mdp_fai.conf
```
Pensez aussi à installer les modules sasl si ils ne sont pas déjà présents sur votre système.

```shell
sudo apt install libsasl2-modules sasl2-bin
```

###### [Revenier en haut](#top) 

# Étape 6
#### Surveillance des logs avec fail2ban

Chacun des ports ouverts est une faille à surveiller en plus.
Heureusement, des outils permettent de parcourir les logs automatiquement à la recherche d'activités suspectes.
Je vais me focaliser sur fail2ban car il fait parti des plus utilisés.

Une fois qu'une activité suspecte est détectée, l'IP responsable va être bannie temporairement.
On peut modifier la rigueur, comme le nombre de tentative, le temps de bannissement, et autre.

On commence par l'installer

```shell
sudo apt install fail2ban
```

Pour commencer à l'utiliser, il faut créer une copie de la configuration de base.
Cette procédure permet d'éviter que le fichier soit modifié lors de mise à jour de l'application.


```shell
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```

On modifie ensuite les paramètres pour ssh et apache:

```shell
[sshd]
enabled = true
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s

[apache-auth]
enabled = true
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
```

Fail2ban propose aussi de surveiller d'autres services, mais je ne vais pas en parler ici.

J'aimerais aussi recevoir des notifications des décisions prises.
Fail2ban possède déjà une option pour le permettre,
il nécessite seulement un serveur SMPT.

```shell
# destinataire
destemail = yoann.truchy@gmail.com

# utiliser postfix
mta = mail
```

D'autres options sont possibles mais je ne m'attarderais pas dessus.

###### [Revenier en haut](#top) 

# Étape 7
#### Redémarrage des services
Avant d'aller plus loin, on redémarre les services que l'on a modifié avant.
```shell
sudo systemctl restart apache2
sudo systemctl restart postfix
sudo systemctl restart sshd
sudo systemctl restart fail2ban
```
#### Installation de Wordpress

Il est possible de construire un site web de plusieurs manières.
On peut tout écrire de A à Z, en écrivant nous même les pages HTML, le CSS et le code PHP.
Or, cette méthode demande beaucoup de temps et exige un grand nombre de connaissances.
C'est pour cela que j'ai choisi d'utiliser Wordpress.
Il s'agit d'un outil qui permet de publier des sites internet,
il possède de nombreux plugins,
et de plus Wordpress est très répandu.
Il possède donc une grande communauté pour chaque demande d'aide.

Pour l'installer **à la racine du site** il faut entrer quelques commandes

```shell
cd /var/www/<domaine>
wget https://wordpress.org/latest.tar.gz
tar -xvf latest.tar.gz
```

Il faut ensuite créer une base de données et un utilisateur qui a tous les droits dessus.

```mysql
CREATE DATABASE <nom> DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci; 
GRANT ALL ON <nom>.* TO '<user>'@'localhost' IDENTIFIED BY '<MotDePasse>';
FLUSH PRIVILEGES;
EXIT;
```

Il ne reste plus qu'à suivre l'installation sur un navigateur web

`https://<domaine>`


# Étape 8
#### Installation du stockage Cloud
Pour le stockage Cloud, j'ai choisi d'utiliser Nextcloud,
car c'est une solution avec des extensions, des clients pour chaque OS, et une communauté active.
On commence par installer les modules php nécessaires.

```shell
sudo apt install php-zip libphp-dom php-mbstring php-gd php-curl php-xml php-apcu php-imagick php-intl
```
On peut maintenant installer Nextcloud

```shell
cd /var/www/
wget https://download.nextcloud.com/server/releases/nextcloud-18.0.4.tar.bz2
tar -xvf nextcloud-18.0.4.tar.bz2
sudo chown -R www-data:www-data /var/www/nextcloud/
```

On modifie ensuite le fichier `/etc/apache2/sites-available/nextcloud.conf`
```apache
Alias /nextcloud "/var/www/nextcloud/"

<Directory /var/www/nextcloud/>
  Options +FollowSymlinks
  AllowOverride All

 <IfModule mod_dav.c>
  Dav off
 </IfModule>

 SetEnv HOME /var/www/nextcloud
 SetEnv HTTP_HOME /var/www/nextcloud

</Directory>
```

Enfin, on applique la configuration et les modules nécessaires:
```apache
sudo a2ensite nextcloud.conf
sudo a2enmod rewrite
sudo a2enmod headers
sudo a2enmod env
sudo a2enmod dir
sudo a2enmod mime
```
Une fois fait, on continue l'installation sur le navigateur, à l'adresse suivante:
`https://<domaine>/nextcloud`

###### [Revenier en haut](#top) 

# Et après

Voilà un an que mon serveur est en place, il à donc été pensé et déployer avec mes connaissances de l'époque.
Il serait judicieux de reprendre l'infrastructure de zéro, et de la reconstruire.
J'aimerais mettre en place les fonctionalitées suivantes:

-  Reverse proxy
-  Containers pour chaque service
-  Simplification de certain URL
-  Meilleur surveillance de mes services
